import random
import itertools

__author__ = 'lmizuhara'

'''
This module implements a simple genetic algorithm framework.
The genetic algorithm functions on populations (lists) of strings with
a given domain (single length strings).

We implement a simple mutation and crossover function, but these can
be easily overridden to use one's own.
'''

DEFAULT_CROSSOVER = lambda x, y: (x,y)

class GeneticAlgorithm(object):

    def __init__(self, fitnessFunction, initPopulation, mutateP=0., crossoverF=None):
        '''
        GeneticAlgorithm class houses a population and can run the genetic
        algorithm evolution methods to find fittest individuals over time.
        The population consists of strings of all identical length composed
        of strings in the domain.

        :param fitnessFunction: (function) the fitness function  must evaluate
        over an element in the population and return a float.
        :param initPopulation: (list) the population is represented as a list
        of string. Each string is of the same length and is composed of single
        length strings found in self.domain
        :return: GeneticAlgorithm (object)
        '''
        self.pSize = len(initPopulation)
        self.population = initPopulation
        self.popFitness = None
        self.fitnessF = fitnessFunction
        self.domain = ['0', '1']
        self.mutateP = mutateP
        self.crossoverF = DEFAULT_CROSSOVER if crossoverF is None else crossoverF
        self.nFitnessCalls = 0
        self.nIters = 0

    def mutate(self, x):
        '''
        mutate function takes an element of the population, mutates
        it and returns it. We implement a randomized mutation of at
        most one string component to be relpaced with a string from
        the domain.

        :param x: (string) element of the population, a string made
        of concatenations of strings in the domain.
        :return: (string) mutated version of x
        '''
        if random.random() < self.mutateP:
            xList = list(x)
            ix = random.randint(0, len(x)-1)
            xList[ix] = random.choice(self.domain)
            x = ''.join(xList)
        return x

    def computeFitness(self, population=None):
        '''
        applies the fitness function to all elements of a given population
        using the fitness function self.fitnessF

        :param population: (list) list representing a population, if no list
        is given, defaults to object's self.population
        :return: (list) list of fitness values (float) of each element of the population
        '''
        if population is None:
            population = self.population
        self.nFitnessCalls += len(population)
        self.popFitness = [self.fitnessF(k) for k in population]
        return self.popFitness

    def evolve(self, population=None):
        '''
        evolves the population one iteration by first applying the crossover function
        over the fittest half, then mutating the new breed and adding it back into the
        fit half of the old population, discarding the weak half of the original
        population

        :param population: (list) list representing a population, if no list
        is given, defaults to object's self.population
        :return: (list) new population
        '''
        if population is None:
            population = self.population
        popTuple = self._getFittest(population)
        fittest = popTuple[self.pSize/2:]
        newBreed = [self.crossoverF(fittest[x][0], fittest[x+1][0]) for x in range(0, len(fittest)-1, 2)]
        newBreed = [self.mutate(x)  for x in list(itertools.chain.from_iterable(newBreed))]
        oldBreed = list(zip(*popTuple[-(self.pSize - len(newBreed)):])[0])
        return oldBreed + newBreed

    def _getFittest(self, population):
        '''
        gets the population in fitness order

        :param population: (list) list representing a population, if no list
        is given, defaults to object's self.population
        :return: (list, list) sorted list of fitness values, sorted population
        '''
        fitness = self.computeFitness(population)
        popTuple = sorted(zip(population, fitness), key=lambda x: x[1])
        return popTuple

    def getFittest(self, k=1):
        '''
        gets the k fittest elements of self.population as a tuple of fitness
        values and population elements

        :param k: (int) number of elements from the population desired
        :return: (list, list) returns both a sorted list of fitness values
        of length k, and the sorted population of length k
        '''

        return self._getFittest(self.population)[-k:]

    def evolveToConvergence(self, tol=0.01, maxIter=1000, verbose=False):
        '''
        applies evolution steps until convergence onto fittest members

        :param tol: (float) tolerance between successive evolution steps
        before termination
        :param maxIter: (int) max number of iterations before stopping
        :param verbose: (boolean) flag for verbose print output, prints
        tuples of iteration number and score
        :return: None
        '''
        score = 100000.
        currScore = 0.
        i=0
        while (abs(score - currScore) > tol) and (i <= maxIter):
            score = currScore
            self.population = self.evolve()
            currScore = sum(self.popFitness)
            i += 1
            if verbose:
                print i, currScore

import random
from math import log
from prims import prim

__author__ = 'lmizuhara'
'''
Implemenation of Charles Isbell's MIMIC algorithm for discrete problems.
Tried not to use numpy. It is faster, but for some a pain to install.
'''

class MIMIC(object):
    ''' Implements MIMIC local random search algorithm for a discrete domain
    http://www.cc.gatech.edu/~isbell/papers/isbell-mimic-nips-1997.pdf
    we use strings units as our domain, so a given point is a string of length N
    '''
    ZERO_EQUIV = 1e-5

    def __init__(self, x0, f, domain=['0', '1'], pSize=10):
        '''
        MIMIC object ontains population over which iterations are conducted. this
        implementation only acts of populations (lists) of strings of identical length

        :param x0: (string) exmaple value of population
        :param f: (function) fitness function that takes a string and returns a float
        :param domain: (list) list of single length strings representing the domain
        that is used to create population element strings of longer length
        :param pSize: (int) population size to generate and iterate over
        :return: MIMIC (object)
        '''
        self.x0 = x0
        self.fitnessF = f
        self.domain = domain
        self.xLen = len(x0)
        self.pSize = pSize
        #we keep the population in list of list mode although we will ultimately return a string join
        self.population = [[random.choice(domain) for k in range(self.xLen)] for j in range(self.pSize)]
        self.probabilities = {}
        self.nFitnessCalls = 0
        self.iter = 0
        self.thetaF = lambda popFitness: popFitness[int(pSize/2)]

    def getAverageFitness(self):
        '''
        gets the average fitness in self.population
        :return: (float)
        '''
        return float(sum([self.fitnessF(''.join(i)) for i in self.population]))/self.pSize

    def getMostFit(self, k=1):
        '''
        finds the k most fit individuals in the population

        :param k: (int)
        :return: (string)
        '''
        items = [''.join(i) for i in self.population]
        fitness = [self.fitnessF(i) for i in items]
        popTuple = sorted(zip(items, fitness), key=lambda x: x[1])
        return popTuple[-k:]

    def run(self, tol=0.01, maxIter=1000, verbose=False):
        '''
        runs the MIMIC algorithm to convergence

        :param tol: (float) tolerance needed to assume convergence of algorithm
        :param maxIter: (int) maximum number of iterations before stopping
        :type verbose: (boolean) flag for printing theta per iteration
        :return: None
        '''
        self.iter = 0
        fitnessRange = 1e6
        #set theta and increase each iteration
        theta = -1e6
        while (fitnessRange > tol) and (self.iter < maxIter):
            #calculate fitnesses of population
            fitnesses = [self.fitnessF(''.join(k)) for k in self.population]
            self.nFitnessCalls += len(self.population)
            #retain those above theta
            keep = [fit > theta for fit in fitnesses]
            aboveThreshold = [self.population[i] for i in range(self.pSize) if keep[i]]
            if aboveThreshold == []:
                break
            #estimate distribution of P(x, theta) by finding max span tree (prim's algo)
            tree = self.findTree(aboveThreshold)
            #use tree as dependency tree to produce a population following that distribution
            self.population = [self.generateMemberFromDepTree(tree) for k in range(self.pSize)]
            theta = self.thetaF(fitnesses) # for now we do binary search to increase theta
            fitnessRange = max(fitnesses) - min(fitnesses) #we test tolerance on how big the range is of fitnesses
            self.iter += 1
            if verbose:
                print self.iter, theta, fitnessRange

    def generateMemberFromDepTree(self, tree):
        '''
        takes a dependence tree and generates a population member following the
        bayes net implied by the dependence tree. Assumes that in calculating the
        tree self.probabilities was filled with the empirical probabilities
        observed in the population

        :param tree: (list) list representing the parent of each element
        :return: (string) random new population member generated from bayes net
        '''
        member = [self.domain[0] for k in range(self.xLen)]
        toTraverse = [tree.index(None)]
        traversed = []
        while len(toTraverse) > 0:
            currentNode = toTraverse[0]
            rval = random.random()
            cumProb = 0
            domainIx = 0
            while rval > cumProb:
                if domainIx==2:
                    print domainIx
                domainVal = self.domain[domainIx]
                parent = tree[currentNode]
                if parent is not None:
                    parentVal = member[parent]
                    cumProb += self.probabilities['j%s,%s' %(currentNode, parent)][domainVal+parentVal] / self.probabilities[parent][parentVal]
                else:
                    cumProb += self.probabilities[currentNode][domainVal]
                domainIx += 1
            member[currentNode] = domainVal
            traversed.append(currentNode)
            children = [i for i, x in enumerate(tree) if x == currentNode]
            toTraverse = toTraverse[1:]
            toTraverse += children
        return member

    def transposePopulation(self, sample):
        '''
        takes the population and produces a list of attributes that appear
        in each location of the population. That is, if an element of the
        population is of length N and there are P elements of the population,
        this should return a list of length N. Each location i of the list is
        another list of length P with all the elements of the domain found in
        the population at that location, i, in the string.

        :param sample: (list) list of subpopulation
        :return: (list) list of attribute lists
        '''
        popAttributes = []
        for i in range(self.xLen):
            attribute = []
            for j in range(len(sample)):
                attribute.append(sample[j][i])
            popAttributes.append(attribute)
        return popAttributes

    def findTree(self, sample):
        '''
        builds a graph, fully connected between features, then
        finds the maximum spanning tree between attributes with
        mutual information as edge weights
        stores probability density functions computed for later use

        :param sample: (list) list of subpopulation on which to build tree
        :return: (list) a list representing the tree structure, the ith
        element of the list represents the parent of node i
        '''
        v = range(self.xLen)
        A = [[0 for k in range(self.xLen)] for j in range(self.xLen)]
        attributes = self.transposePopulation(sample)
        for i in range(self.xLen):
            for j in range(i, self.xLen):
                joint = {'%s%s'%(k,l):self.ZERO_EQUIV for k in self.domain for l in self.domain} #joint probability density
                joint2 = {'%s%s'%(k,l):self.ZERO_EQUIV for k in self.domain for l in self.domain} #joint probability density
                p1 = {k:self.ZERO_EQUIV for k in self.domain} #marginal density 1
                p2 = {k:self.ZERO_EQUIV for k in self.domain} #marginal density 2
                vecLen = len(attributes[i])
                for k in range(vecLen):
                    myKey = attributes[i][k] + attributes[j][k]
                    addProbToDict(myKey, joint, vecLen)
                    #we enter both i,j and j,i as keys because joint probabilities are symmetric
                    myKey2 = attributes[j][k] + attributes[i][k]
                    addProbToDict(myKey2, joint2, vecLen)
                    #while it seems odd to calculate the marginals in the inner loop like this, we do it here because
                    # we need to make sure both values are in place for the mutual information calculation
                    addProbToDict(attributes[i][k], p1, vecLen)
                    addProbToDict(attributes[j][k], p2, vecLen)
                self.probabilities[i] = p1
                self.probabilities[j] = p2
                self.probabilities['j%s,%s'%(i,j)] = joint
                self.probabilities['j%s,%s'%(j,i)] = joint2
                # one liner for calculating mutual information over discrete density
                mi = sum([sum([joint[x+y]*log(joint[x+y] / (p1[x]*p2[y])) for x in p1.keys()]) for y in p2.keys()])
                A[i][j] = mi
                A[j][i] = mi
        return prim(v, A, 0)

def addProbToDict(k, d, normalizer):
    ''' adds a probability of a discrete event to a dictionary carrying probabilities

    :param k: (string) key
    :param d: (string) dictionary of probabilities
    :param normalizer: (int) number of events to be put into the dictionary
    :return: None
    '''
    if k in d.keys():
        d[k] += 1./normalizer
    else:
        d[k] = 1./normalizer


from ga import GeneticAlgorithm

__author__ = 'lmizuhara'

import unittest


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.testPop = ['001010', '101100', '000000']
        self.objF = lambda x: int(x)
        self.ga = GeneticAlgorithm(self.objF, self.testPop, .5)

    def tearDown(self):
        pass

    def testMutate(self):
        print 'testMutate'
        import random
        random.seed(1245)
        self.assertEqual(self.ga.mutate('0000000'), '0000000')
        self.assertEqual(self.ga.mutate('1111111'), '1111111')
        random.seed(12345)
        self.assertEqual(self.ga.mutate('0000000'), '1000000')
        self.assertEqual(self.ga.mutate('1111111'), '1101111')

    def testComputeFitness(self):
        print 'testComputeFitness'
        assert self.ga.computeFitness() == [1010, 101100, 0]

    def testGetFittest(self):
        print 'testGetFittest'
        assert self.ga._getFittest(self.testPop) == [('000000', 0), ('001010', 1010), ('101100', 101100)]
        assert self.ga.getFittest() == [('101100',  101100)]

    def testEvolve(self):
        print 'testEvolve'
        ans = self.ga.evolve()
        assert len(ans) == 3

    def testEvolveToConvergene(self):
        print 'testEvolveToConvergence'
        def midptCrossover(x, y):
            midpt = len(x)/2
            return x[0:midpt] + y[midpt:], y[0:midpt] + x[midpt:]
        self.ga.population = ['000111', '111000', '000000', '001001']
        self.ga.crossoverF = midptCrossover
        self.ga.mutateP = 0.
        self.ga.evolveToConvergence()
        assert self.ga.getFittest() == [('111111', 111111)]


if __name__ == '__main__':
    unittest.main()

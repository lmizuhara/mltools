__author__ = 'lmizuhara'

import unittest, random
from mimic import MIMIC, addProbToDict


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.algo = MIMIC('111', f=lambda x: int(x))

    def tearDown(self):
        pass

    def testTransposePopulation(self):
        print 'testTransposePopulation'
        sample = ['010', '111', '000']
        self.algo.xLen = 3
        myTranspose = [['0', '1', '0'], ['1', '1', '0'], ['0', '1', '0']]
        assert self.algo.transposePopulation(sample) == myTranspose

    def testAddProbabilityToDict(self):
        print 'testAddProbToDict'
        coinDict = {}
        addProbToDict('heads', coinDict, 4)
        addProbToDict('heads', coinDict, 4)
        addProbToDict('heads', coinDict, 4)
        addProbToDict('tails', coinDict, 4)
        assert coinDict['heads'] == .75
        assert coinDict['tails'] == .25

    def testFindTree(self):
        print 'testFindTree'
        testSample = ['010', '111', '000']
        self.algo.xLen = 3
        assert self.algo.findTree(testSample) == [None, 0, 1]
        assert self.algo.probabilities.keys() == [0, 1, 2, 'j0,2', 'j0,0', 'j0,1', 'j2,0', 'j2,1', 'j2,2', 'j1,2', 'j1,1', 'j1,0']

    def testGenerateMemberFromDepTree(self):
        print 'testGenerateMemberFromDepTree'
        testSample = ['010', '110', '010']
        self.algo.xLen = 3
        tree = self.algo.findTree(testSample)
        members = [self.algo.generateMemberFromDepTree(tree) for i in range(100)]
        assert isinstance(members[0], list)
        assert len(members[0]) == 3
        attributes = self.algo.transposePopulation(members)
        assert sum([int(i) for i in attributes[0]]) >= 0
        assert sum([int(i) for i in attributes[1]]) == 100
        assert sum([int(i) for i in attributes[2]]) == 0

    def testGetAverageFitness(self):
        print 'testGetAverageFitness'
        self.algo.population = ['010', '101', '111']
        assert self.algo.getAverageFitness() == 22.2

    def testGetMostFit(self):
        print 'testGetMostFit'
        self.algo.population = ['010', '101', '111']
        assert self.algo.getMostFit() == [('111', 111)]

    def testRun(self):
        print 'testRun'
        algo = MIMIC('111000', f=lambda x: int(x), pSize=100)
        algo.run(verbose=False)
        assert algo.getAverageFitness() >= 100.

    def testFourPeaks(self):
        print 'testFourPeaks'
        def leading1s(x):
            isLeading = True
            i = 0
            while isLeading:
                isLeading = x[i] == '1'
                i += 1
                if i==len(x):
                    return i
            return i-1

        def trailing0s(x):
            isTrail = True
            i = 1
            while isTrail:
                isTrail = x[-i] == '0'
                if i==len(x):
                    return i
                i += 1
            return i-2

        def r(x):
            if leading1s(x)>4 and trailing0s(x)>4:
                return len(x)
            else:
                return 0

        def fourPeaks(x):
            return max([leading1s(x), trailing0s(x)]) + r(x)
        random.seed(13)
        x0 = ''.join([random.choice(self.algo.domain) for a in range(40)])
        mimic = MIMIC(x0, fourPeaks, pSize=100)
        mimic.run()
        print mimic.getMostFit()



if __name__ == '__main__':
    unittest.main()

import random
from localRandomSearch import getNeighbors, discreteStep, discreteHillClimb, simulatedAnnealing, \
    randomRestartHillClimb

__author__ = 'lmizuhara'

import unittest


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.x0 = '01001010'
        self.f = lambda x: int(x)
        self.max = ('11111111', 11111111)

    def tearDown(self):
        pass

    def testGetNeighbors(self):
        print 'testGetNeighbors'
        assert len(getNeighbors(self.x0)) == len(self.x0)

    def testDiscreteStep(self):
        print 'testDiscreteStep'
        assert discreteStep(self.f, self.x0) == ('11001010', 11001010)

    def testDiscreteHillClimb(self):
        print 'testGetNeighbors'
        assert discreteHillClimb(self.f, self.x0) == self.max

    def testRandomRestartHillClimb(self):
        print 'testRandomRestartHillClimb'
        assert randomRestartHillClimb(self.f, self.x0) == self.max
        random.seed(1904)
        output = randomRestartHillClimb(self.f, self.x0, trackCalls=True)
        assert output[0:2] == self.max
        assert output[2] == 3237

    def testSimulatededAnnealing(self):
        print 'testSimulatedAnnealing'

        target = '011110'*10
        f = lambda x: sum([it==target[i] for i,it in enumerate(x)])
        x = '100001'*10
        tempSmall = lambda i, maxIter: (maxIter-i)/float(maxIter)*len(target)/8.
        tempLarge = lambda i, maxIter: (maxIter-i)/float(maxIter)*len(target)
        numTries = 100

        random.seed(1235)
        simTempSmall = [f(simulatedAnnealing(f, x, 60*2, tempFunction=tempSmall)[0]) for k in range(numTries)]
        simTempLarge = [f(simulatedAnnealing(f, x, 60*2, tempFunction=tempLarge)[0]) for k in range(numTries)]

        averageFitnessTempSmall = sum(simTempSmall)/numTries
        averageFitnessTempLarge = sum(simTempLarge)/numTries

        #given f is monotonic, we expect slower convergence when temperature is high from allowing many false positives
        self.assertLess(averageFitnessTempLarge, averageFitnessTempSmall)

if __name__ == '__main__':
    unittest.main()

from copy import copy
import random
import math

__author__ = 'lmizuhara'
'''
This module finds maxima using local random search techniques for functions over discrete spaces.
Each discrete position is represented by a string where each position of the string can be altered within a
domain of single length strings. For example a bit string '010101001' could be a discrete value with a domain
list ('0', '1'). All the local search functions default to a search space of bit strings. We could, for example,
search a space of 5 combinations of 4 possible values by using a domain of ['a', 'b', 'c', 'd']. An example
value of one of these might be 'bbcad'.
'''

def getNeighbors(x0, domain=('0', '1')):
    ''' gets all neighbors to a point x0. Neighbors are defined as all strings where only one point is altered

    :param x0: (str) a string representing a starting position
    :param domain: (list) list of strings, each string should be of length 1

    :return: (list) a list of all strings of length equal to the length of x0 with exactly one position altered
    '''
    neighborList = []
    xList = [x for x in x0]
    for i in range(len(xList)):
        for k in domain:
            if xList[i] != k:
                nextList = copy(xList)
                nextList[i] = k
                neighborList.append(''.join(nextList))
    return neighborList

def discreteStep(f, x0, domain=('0', '1')):
    ''' finds the neighbor string to x0 that maximizes the value of the function f

    :parmm f: (function) a function that takes a string of length len(x0) and returns a float
    :param x0: (str) a string representing a starting position
    :param domain: (list) list of strings, each string should be of length 1

    :return: (str, float) a string neighbor of x0 and its value f(x),
    returns x0 and f(x0) if f(x0)>f(x) for all neighbors x of x0
    '''
    neighborList = getNeighbors(x0, domain=domain)
    valList = [f(x) for x in neighborList]
    val, currStr = max(zip(valList, neighborList))
    fx0 = f(x0)
    if val < fx0:
        return x0, fx0
    else:
        return currStr, val

def discreteHillClimb(f, x0, domain=('0', '1'), tol=.01, maxIter=1000, trackCalls=False):
    ''' runs a basic discrete hill climb routine to find max(f)

    :parmm f: (function) a function that takes a string of length len(x0) and returns a float
    :param x0: (str) a string representing a starting position
    :param domain: (list) list of strings, each string should be of length 1
    :param tol: (float) tolerance for stopping the hill climb
    :param maxIter: (int) number of iterations to time out if no maximum is found before nIter iterations
    :param trackCalls: (bool): if True returns number of iterations and number of fitness Function calls

    :return: (str, float) a string value that maximizes the function f
             (str, float, int, int) if trackCalls is True, also return number of iterations and fitness calls
    '''
    val = f(x0)
    nextStr, nextVal = discreteStep(f, x0, domain)
    nIter = 0
    nCalls = 1
    while (abs(nextVal-val) > tol) and (nIter < maxIter):
        val = nextVal
        nextStr, nextVal = discreteStep(f, nextStr, domain=domain)
        if trackCalls:
            nCalls += (len(domain)-1) * len(x0) #discrete steps calls fitness function once per neighbor
        nIter += 1
    if trackCalls:
        return nextStr, val, nIter, nCalls
    else:
        return nextStr, val

def randomRestartHillClimb(f, x0, nRestarts=100, domain=('0', '1'), tol=0.01, maxIter=1000, trackCalls=False):
    ''' runs random restart algorithm for a basic discrete hill climb routine to find max(f)

    :parmm f: (function) a function that takes a string of length len(x0) and returns a float
    :param x0: (str) a string representing a starting position
    :param nRestarts: (int) number of times to random restart the hill climb
    :param domain: (list) list of strings, each string should be of length 1
    :param tol: (float) tolerance for stopping the hill climb
    :param maxIter: (int) number of iterations to time out if no maximum is found before nIter iterations
    :param trackCalls: (bool): if True returns number of fitness Function calls

    :return: (str, float) a string value that maximizes the function f
    '''
    nCalls = 0
    output = discreteHillClimb(f, x0, domain=domain, tol=tol, maxIter=maxIter, trackCalls=trackCalls)
    bestStr, bestVal = output[0], output[1]
    if trackCalls:
        nCalls = output[3]
    for i in range(nRestarts):
        strList = [random.choice(domain) for k in range(len(x0))]
        output = discreteHillClimb(f, ''.join(strList), domain=domain, tol=tol, maxIter=maxIter, trackCalls=trackCalls)
        x1, val = output[0], output[1]
        if trackCalls:
            nCalls += output[3]
        if val > bestVal:
            bestVal = val
            bestStr = x1
    if trackCalls:
        return bestStr, bestVal, nCalls
    return bestStr, bestVal

def simulatedAnnealing(f, x0, nIter=1000, domain=('0', '1'),
                       tempFunction=lambda i, maxIter: (maxIter -i)/float(maxIter),
                       trackCalls = False):
    ''' runs random restart algorithm for a basic discrete hill climb routine to find max(f)

    :parmm f: (function) a function that takes a string of length len(x0) and returns a float
    :param x0: (str) a string representing a starting position
    :param nIter: (int) number of times to random restart the hill climb
    :param domain: (list) list of strings, each string should be of length 1
    :param tempFunction: (function) a function that returns the temperature value given
    lambda i, nIter: -> R
        where i is the iteration
        where nIter is the max number of iterations
        this function should be monotone decreasing
    :param trackCalls: (bool): if True returns number of fitness Function calls

    :return: (str, float) a string value that maximizes the function f
    '''
    x = x0
    nCalls = 0
    for i in range(nIter):
        temp = tempFunction(i, nIter)
        xList = [v for v in x]
        ix = random.randint(0, len(x0)-1)
        xList[ix] = random.choice(domain)
        randomNeighbor = ''.join(xList)
        fx = f(x)
        fr = f(randomNeighbor)
        nCalls +=2
        if fr > fx:
            p = 1.
        else:
            p = math.exp((fr-fx)/temp)
        if random.random() <= p:
            x = randomNeighbor
    if trackCalls:
        return x, fx, nCalls
    else:
        return x, fx

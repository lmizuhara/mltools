# README #

### Machine Learning Tools ###

* Machine learning algorithms for GaTech's ML7641 course. Algorithms used in the course in Spring 2015 but not implemented in [Scikit-learn](http://scikit-learn.org/stable/index.html) have been implemented here. No guarantees on correctness or efficiency of algorithm implementation are made; however, I do believe they work.
* Version 0.1

### Implemented Algorithms ###

* Discrete Function Hill Climbing
* Discrete Random Restart Hill Climb
* Discrete Simulated Annealing
* Genetic Algorithm 
* MIMIC

### Feedback ###

* Feel free to give feedback
* Each module has its own test file, do check that out for use examples

### Who do I talk to? ###

* Leo Mizuhara